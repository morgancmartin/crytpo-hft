FROM java:8-alpine
MAINTAINER Your Name <you@example.com>

ADD target/uberjar/crypto-hft.jar /crypto-hft/app.jar

EXPOSE 3000

CMD ["java", "-jar", "/crypto-hft/app.jar"]
