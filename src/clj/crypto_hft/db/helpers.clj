(ns crypto-hft.db.helpers
  (:require
   [crypto-hft.db.core :as db]
   [crypto-hft.helpers :as helpers]
   [crypto-hft.config :refer [env]]
   ))

(defn get-last-trade! [minute-window]
  (first (db/get-last-trade {:minute_window minute-window})))

(defn create-ticker! [msg]
  (try
    (db/create-ticker! msg)
    (catch Exception e (helpers/handle-exception! e))))

(defn create-rsi! [info]
  (if (helpers/persist-data env)
    (db/create-rsi! info)))

(defn get-tickers-by-minutes! [minutes]
  (db/get-tickers-since {:begin_time (helpers/past-timestamp-by-minute minutes)}))

(defn get-trades! [minute-window]
  (db/get-trades {:minute_window minute-window}))

(defn get-last-rsi! [minute-window]
  (try
    (first (db/get-last-rsi {:minute_window minute-window}))
    (catch Exception e (helpers/handle-exception! e))))

(defn create-sell! [info]
  (let [last-trade (:last-trade info)
        info (conj info {:type "sell"})]
    (db/create-trade! info)))

(defn create-buy! [info]
  (let [last-trade (:last-trade info)
        info (conj info {:type "buy"})]
    (db/create-trade! info)))
