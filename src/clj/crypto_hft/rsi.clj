(ns crypto-hft.rsi)

(defn ticker-col-price-average [tickers]
  (/ (reduce #(+ %1 (get %2 :price)) 0 tickers)
     (count tickers)))

(defn ticker-price-changes [tickers]
  (map #(- (second %) (first %)) (partition 2 1 (map :price tickers))))

(defn ticker-loss-gain [tickers]
  (let [changes (ticker-price-changes tickers)]
    {:gains (filter pos? changes)
     :losses (filter neg? changes)}))

(defn relative-strength [tickers]
  (let [{:keys [gains losses]} (ticker-loss-gain tickers) total (count tickers)]
    (/ (/ (reduce + gains) total)
       (/ (Math/abs (reduce + losses)) total))))

(defn relative-strength-index [tickers]
  (- 100 (/ 100 (+ 1 (relative-strength tickers)))))

(defn rsi-by-minutes [tickers minutes]
  (if (> (count tickers) 1)
    (relative-strength-index tickers)))

(defn should-buy [buy-data]
  (let [{:keys [last-rsi buy-rsi new-rsi]} buy-data]
    (if (and (< last-rsi buy-rsi)
             (> new-rsi buy-rsi)
             (= (:type (:last-trade buy-data)) "sell"))
      true
      false)))

(defn should-sell [sell-data]
  (let [{:keys [last-rsi sell-rsi new-rsi]} sell-data]
    (if (and (> last-rsi sell-rsi)
             (< new-rsi sell-rsi)
             (= (:type (:last-trade sell-data)) "buy"))
      true
      false)))
