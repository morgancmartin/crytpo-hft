(ns crypto-hft.gdax
  (:require [gniazdo.core :as ws]
            [clj-time.format :as f]
            [crypto-hft.db.core :as db]
            [crypto-hft.db.helpers :as dbh]
            [crypto-hft.config :refer [env]]
            [crypto-hft.helpers :as helpers]
            [crypto-hft.rsi :as rsi]
            [mount.core :refer [defstate]]
            [clojure.tools.logging :as log]
            [clojure.core.async :as async]
            [pandect.algo.sha256 :refer :all]
            [clj-http.client :as client]
            [slingshot.slingshot :refer [throw+ try+]]
            [clojure.string :as str]
            [clojure.java.shell :refer [sh]]
            )
  (:import [java.io StringReader BufferedReader]
           ))

;; BINDINGS ;;
(def ws-timeout nil)
(def sell-rsi 72)
(def buy-rsi 30)
;; (def trades (atom []))
(def gdax-secret "Z0P2LlEgRggxPsX3J5VK1cQ7W3mP7J0JusknN3IDJzLKUg5ueiHLI82C6wQfBZx0m8uMYsAiQlBj3oBtnHBVBA==")
(def gdax-key "2fafd32c420d755646b2c325d3fefe80")
(def gdax-pass "q4d1xpjtgxc")
(def http-endpoint "https://api.gdax.com")
(def ws-endpoint "wss://ws-feed.gdax.com")
(def output-file "output.txt")
(def socket nil)
(def max-text-message-size 250000)
(def subscribe-request
  (helpers/to-json {
            "type" "subscribe",
            "product_ids" [
                           "BTC-USD"
                           ],
            "channels" [
                        "level2",
                        "heartbeat",
                        {
                         "name" "ticker",
                         "product_ids" [
                                        "BTC-USD"
                                        ]
                         },
                        ]
            }))
(def client (ws/client (new java.net.URI ws-endpoint)))
(.setMaxTextMessageSize (.getPolicy client) max-text-message-size)
(.start client)

(defn get-gdax-sig-and-stamp [secret method path body]
  (let [result (str/split (:out (sh "sig-maker/index1.js"
                                    (str "--s=" secret)
                                    (str "--m=" method)
                                    (str "--p=" path)
                                    (str "--b=" body))) #"\s")]
    (do
      {:timestamp (nth result 0)
       :signature (nth result 1)})))


;; (defn make-gdax-sig [timestamp method request-path body secret]
;;   (let [secret (from-b64 secret)
;;         body (to-json body)
;;         what (str timestamp method request-path body)]
;;     (to-b64 (sha256-hmac what secret))))

;; (defn open-buy? [trades]
;;   (let [last-buy (last trades)]
;;     (and (> (count trades) 0)
;;          (:open last-buy))))


;; IMPURE FUNCTIONS ;;

(defn get-gdax-headers [timestamp sig]
  {:headers
   {"CB-ACCESS-KEY" gdax-key
    "CB-ACCESS-SIGN" sig
    "CB-ACCESS-TIMESTAMP" (str timestamp)
    "CB-ACCESS-PASSPHRASE" gdax-pass}})

(defn gdax-get-request [req-endpoint]
  (let [{:keys [signature timestamp]}
        (get-gdax-sig-and-stamp
         gdax-secret
         "GET"
         req-endpoint
         "")]
    (client/get
     (str http-endpoint req-endpoint)
     (get-gdax-headers timestamp signature))))

;; from-json/to-json work correctly, i.e. they give the same output as JSON.stringify
;; should be able to convert POST body back over to clojure code
;; fuck I'm happy I got it working :)
(defn gdax-post-request [req-endpoint body]
  (let [
        body (helpers/to-json body)
        {:keys [signature timestamp]}
        (get-gdax-sig-and-stamp
         gdax-secret
         "POST"
         req-endpoint
         body
         )
        request
        (assoc
         (get-gdax-headers timestamp signature)
         :body body
         :throw-entire-message? true
         :content-type :json)]
    ;; (try+
     (do
       (prn (helpers/from-json body))
       (prn (str http-endpoint req-endpoint))
       (prn request)
       (client/post
        (str http-endpoint req-endpoint)
        request))
     ;; (catch Object _ (log/error (:throwable &throw-context) "unexpected error") (throw+))
     ))

(defn gdax-get-accounts []
  (helpers/from-json (:body (gdax-get-request "/accounts"))))

(defn get-account-balances []
  (let [response (gdax-get-accounts)]
    {:usd-bal (bigdec
               (get-in
                (helpers/find-rec response "currency" "USD")
                ["available"]))
     :btc-bal (bigdec
               (get-in
                (helpers/find-rec response "currency" "BTC")
                ["available"]))
     }))

(defn gdax-buy-btc [amount-in-usd]
  (gdax-post-request
   "/orders"
   {:type "market" :side "buy" :product_id "BTC-USD"
    :funds amount-in-usd}
   ))

(defn gdax-sell-btc [amount-in-btc]
  (gdax-post-request
   "/orders"
   {:type "market" :side "sell" :product_id "BTC-USD"
    :size amount-in-btc}
   ))


(defn set-timeout! [secs callback]
  (let [closed (atom false)]
    (async/go
      (async/<! (async/timeout (* 1000 secs)))
      (if (not @closed)
        (callback)))
    #(reset! closed true)))

(defn reset-output-file! [] (spit output-file ""))

(defn output-to-file! [val]
  (do
    (spit output-file val :append true)
    (spit output-file "\n" :append true)))


(defn handle-buy! [info]
  (let [amount (helpers/round 2 (:usd-bal (get-account-balances)))]
    (do
      (gdax-buy-btc amount)
      (dbh/create-buy! (conj info {:amount amount}))
      (prn "BUYING: " info)
      (newline))))

;; (defn close-last-buy! [trades]
;;   (let [last-buy-index (- (count @trades) 1)
;;         last-buy (last @trades)
;;         closed-buy (assoc last-buy :open false)]
;;     (swap! trades assoc last-buy-index closed-buy)))

(defn handle-sell! [info]
  (let [amount (:btc-bal (get-account-balances))]
    (do
      (gdax-sell-btc amount)
      (dbh/create-sell! (conj info {:amount amount}))
      (prn "SELLING: " info)
      (newline))))

(defn print-info! [info]
  (if (helpers/show-info env)
    (let [{:keys [msg ticker-count trading rsi minute-window]} info]
      (prn 'received msg)
      (prn {:ticker-count ticker-count :trading trading})
      (prn "RSI: " rsi " at minute-window: " minute-window)
      (newline))))

(defn handle-trading! [info]
  (try
    (if (helpers/trading env)
      (do
        (if (rsi/should-buy info)
          (handle-buy! info))
        (if (rsi/should-sell info)
          (handle-sell! info))))
    (catch Exception e (helpers/handle-exception! e))))

(defn handle-rsi! [minute-window last-msg]
  (let [tickers (dbh/get-tickers-by-minutes! minute-window)
        rsi (rsi/rsi-by-minutes tickers minute-window)
        last-rsi (:rsi (dbh/get-last-rsi! minute-window))
        info {
              :rsi rsi :price (:price last-msg)
              :msg last-msg :ticker-count (count tickers)
              :trading (helpers/trading env)
              :valid_time (:valid_time last-msg)
              :new-rsi rsi :last-rsi last-rsi
              :buy-rsi buy-rsi :sell-rsi sell-rsi
              :instrument "BTC-USD"
              :minute_window minute-window
              :minute-window minute-window
              :market_price (:price last-msg)
              :last-trade (dbh/get-last-trade! minute-window)}]
    (do
      (print-info! info)
      (handle-trading! info)
      (dbh/create-rsi! info)
      )))

(defn on-ws-timeout! []
  (do
    (mount.core/stop)
    (set-timeout! 10 #(mount.core/start))))

(defn reset-ws-timeout! []
  (do
    (if ws-timeout (ws-timeout))
    (def ws-timeout
      (set-timeout! (helpers/ws-timeout-in-secs env) on-ws-timeout!))))

(defn on-receive! [msg]
  (let [msg (helpers/get-sql-ready-msg msg)]
    (if (= (get msg :type) "ticker")
      (do
        (reset-ws-timeout!)
        (dbh/create-ticker! msg)
        (try
          (do
            ;; (handle-rsi! 1 msg)
            ;; (handle-rsi! 5 msg)
            (handle-rsi! 15 msg)
            ;; (handle-rsi! 60 msg)
            )
          (catch Exception e (helpers/handle-exception! e)))))))

(defn send-msg! [socket msg] (ws/send-msg socket msg))

(defn on-connect! [msg]
  (prn msg)
  ;; (reset-output-file!)
  (send-msg! socket subscribe-request))

(defn on-error! [t]
  (log/info (:cause (Throwable->map t))))

(defn connect! []
  (ws/connect
   ws-endpoint
   :client client
   :on-receive on-receive!
   :on-connect on-connect!
   :on-error on-error!))

(defn close! []
  (do
    (if ws-timeout (ws-timeout))
    (ws/close socket)))

(defstate socket
  :start (if (helpers/active env)
           (connect!))
  :stop (if (helpers/active env)
          (close!)))

;; (load "crypto_hft/gdax")
;; (in-ns 'crypto-hft.gdax)
;; (use 'crypto-hft.gdax :reload-all)
;; (mount.core/start)
