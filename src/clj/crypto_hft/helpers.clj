(ns crypto-hft.helpers
  (:require
   [clojure.data.codec.base64 :as b64]
   [clojure.walk :as walk]
   [clojure.edn :as edn]
   [clojure.set :as set]
   [clj-json.core :as json]
   [clj-time.coerce :as c]
   [clj-time.core :as t]
   ))

;; MACROS ;;
(defmacro when-let*
  ([bindings & body]
   (if (seq bindings)
     `(when-let [~(first bindings) ~(second bindings)]
        (when-let* ~(drop 2 bindings) ~@body))
     `(do ~@body))))

;; FUNCTIONS ;;
(defn to-json [val] (json/generate-string val))

(defn from-json [val] (json/parse-string val))

(defn handle-exception! [e]
  (prn (str "caught exception: " (.getMessage e))))

(defn round [s n] 
  (.setScale (bigdec n) s java.math.RoundingMode/FLOOR)) 

(defn find-rec [records key val]
  (some #(if (= (get-in % [key]) val) %) records))

(defn secs-since-epoch []
  (quot (System/currentTimeMillis) 1000))

(defn from-b64 [original]
  (String. (b64/decode (.getBytes original))))

(defn to-b64 [original]
  (String. (b64/encode (.getBytes original)) "UTF-8"))

(defn is-starter-trade? [trade]
  (= (:market_price trade) 1.0))

(defn is-type-of? [item key val]
  (if (= (key item) val) item nil))

(defn is-buy-trade? [trade]
  (is-type-of? trade :type "buy"))

(defn is-sell-trade? [trade]
  (is-type-of? trade :type "sell"))

(defn trade-pair-profit [pair]
  (let [buy-trade (some is-buy-trade? pair)
        sell-trade (some is-sell-trade? pair)]
    (- (:market_price sell-trade) (:market_price buy-trade))))

(defn calculate-profit [trades] ;; assumes consistent minute-window
  (let [pairs (partition 2 (filter #(not (is-starter-trade? %)) trades))]
    (reduce + (map trade-pair-profit pairs))))

(defn active [env] (get-in env [:gdax :active]))
(defn trading [env] (get-in env [:gdax :trading]))
(defn show-info [env] (get-in env [:gdax :show-info]))
(defn persist-data [env] (get-in env [:gdax :persist-data]))
(defn ws-timeout-in-secs [env]
  (get-in env [:gdax :ws-timeout-in-secs]))

(defn pare-msg [msg]
  (select-keys (from-json msg) ["type" "price" "time"]))

(defn time-to-sql-time [time]
  (c/to-sql-time (c/from-long (c/to-long time))))

(defn get-sql-ready-msg [msg]
  (let [msg (pare-msg msg)]
    (-> msg
        (walk/keywordize-keys)
        (update-in [:time] time-to-sql-time)
        (update-in [:price] edn/read-string)
        (set/rename-keys {:time :valid_time}))))

(defn past-timestamp-by-minute [num-minutes]
  (t/minus (t/now) (t/minutes num-minutes)))
