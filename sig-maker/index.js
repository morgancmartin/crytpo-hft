#!/usr/bin/env node
const signer = require('gdax/lib/request_signer');

var argv = require('minimist')(process.argv.slice(2));
var auth = {secret: argv.s};
var method = argv.m;
var path = argv.p;
var result = signer.signRequest(auth, method, path);
var sig = result.signature;
var stamp = result.timestamp;
console.log(stamp);
console.log(sig);
