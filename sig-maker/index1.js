#!/usr/bin/env node
var crypto = require('crypto');

var argv = require('minimist')(process.argv.slice(2));
var method = argv.m;
var secret = argv.s;
var requestPath = argv.p;
var body = argv.b;
// var body = '';
// if (method === 'POST') {
//     body = JSON.stringify({
//         type: 'market',
//         side: 'sell',
//         product_id: 'BTC-USD',
//         size: '0.01202155'
//     });
// }
var timestamp = Date.now() / 1000;
var what = timestamp + method + requestPath + body;
var key = Buffer(secret, 'base64');
var hmac = crypto.createHmac('sha256', key);

console.log(timestamp);
console.log(hmac.update(what).digest('base64'));

/* var body = JSON.stringify({
   price: '1.0',
   size: '1.0',
   side: 'buy',
   product_id: 'BTC-USD'
   }); */

