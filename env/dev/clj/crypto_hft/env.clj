(ns crypto-hft.env
  (:require [selmer.parser :as parser]
            [clojure.tools.logging :as log]
            [crypto-hft.dev-middleware :refer [wrap-dev]]))

(def defaults
  {:init
   (fn []
     (parser/cache-off!)
     (log/info "\n-=[crypto-hft started successfully using the development profile]=-"))
   :stop
   (fn []
     (log/info "\n-=[crypto-hft has shut down successfully]=-"))
   :middleware wrap-dev})
