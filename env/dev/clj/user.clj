(ns user
  (:require [luminus-migrations.core :as migrations]
            [crypto-hft.config :refer [env]]
            [mount.core :as mount]
            crypto-hft.core))

(defn start []
  (mount/start-without #'crypto-hft.core/repl-server))

(defn stop []
  (mount/stop-except #'crypto-hft.core/repl-server))

(defn restart []
  (stop)
  (start))

(defn migrate []
  (migrations/migrate ["migrate"] (select-keys env [:database-url])))

(defn rollback []
  (migrations/migrate ["rollback"] (select-keys env [:database-url])))


