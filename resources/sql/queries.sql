-- :name create-user! :! :n
-- :doc creates a new user record
INSERT INTO users
(id, first_name, last_name, email, pass)
VALUES (:id, :first_name, :last_name, :email, :pass)

-- :name update-user! :! :n
-- :doc updates an existing user record
UPDATE users
SET first_name = :first_name, last_name = :last_name, email = :email
WHERE id = :id

-- :name get-user :? :1
-- :doc retrieves a user record given the id
SELECT * FROM users
WHERE id = :id

-- :name delete-user! :! :n
-- :doc deletes a user record given the id
DELETE FROM users
WHERE id = :id

-- :name create-ticker! :! :n
-- :doc creates a new ticker record
INSERT INTO tickers
(price, valid_time)
VALUES (:price, :valid_time)

-- :name get-tickers-since :?
-- :doc gets ticker records since timestamp
SELECT * FROM tickers
WHERE valid_time > :begin_time

-- :name create-rsi! :! :n
-- :doc creates a new ticker record
INSERT INTO rsis
(instrument, rsi, minute_window, market_price, valid_time)
VALUES (:instrument, :rsi, :minute_window, :market_price, :valid_time)

-- :name get-last-rsi :?
-- :doc gets last rsi record by minute window
SELECT * FROM rsis
WHERE minute_window = :minute_window
ORDER BY ID
DESC LIMIT 1

-- :name get-rsis :?
-- :doc gets rsis by minute window
SELECT * FROM rsis
WHERE minute_window = :minute_window
ORDER BY ID

-- :name create-trade! :! :n
-- :doc creates a new trade record
INSERT INTO trades
(type, minute_window, market_price, valid_time, amount)
VALUES (:type, :minute_window, :market_price, :valid_time, :amount)

-- :name get-last-trade :?
-- :doc gets last trade record
SELECT * FROM trades
WHERE minute_window = :minute_window
ORDER BY ID
DESC LIMIT 1

-- :name get-trades :?
-- :doc gets trades records by minute window
SELECT * FROM trades
WHERE minute_window = :minute_window
ORDER BY ID

-- :name get-trades-since :?
-- :doc gets trades records by minute window since timestamp
SELECT * FROM trades
WHERE minute_window = :minute_window
AND valid_time > :begin_time
ORDER BY ID
