CREATE TABLE rsis
(id SERIAL PRIMARY KEY,
 instrument VARCHAR(30),
 rsi REAL,
 minute_window integer,
 market_price REAL,
 valid_time TIMESTAMP);
