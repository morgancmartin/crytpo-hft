CREATE TABLE tickers
(id SERIAL PRIMARY KEY,
 price REAL,
 valid_time TIMESTAMP);
