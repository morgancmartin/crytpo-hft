CREATE TABLE trades
(id SERIAL PRIMARY KEY,
type VARCHAR(30),
minute_window integer,
market_price REAL,
valid_time TIMESTAMP);
